#!/usr/bin/env bash

echo -e "Git Auto\n---\nScript who allows you to automatically create\na gitlab remote repositories and branches.\n---\n";
read -p "Repositories names ( repo1 repo2 ...) : " repositories;

#readme generation function
function myreadme(){

    read -p "Principal contributor (dupont) : " user ;
    read -p "Project name (dupont project): " project ; # ${PWD##*/}  for the current folder
    read -p "Technos (python java ...): " technos ;
    read -p "Your gitlab pseudo (@dupont) :" username ;

    echo "Project: $project " >> README.md ;
    echo "--------------" >> README.md ;
    echo "- Principal contributor : $user " >> README.md ;
    echo "- Folder : $project " >> README.md ;
    echo "- Technologies : $technos" >> README.md ;

    #First commit
    git add README.md ;
    git commit -m "First commit!" ;

    echo "README creation is OK" ;
}


if [ -z "$repositories" ]; then
   echo "Please remote repositories";
else
 
    # test if is a git project
    if [ $(ls -l | grep -c .git)=0 ]; then 
        git init ;
    fi 

    myreadme;
    read -p "Initial branches (production develop ...): " new_branch;

    for depot in $repositories ; do 
        echo "### ----> Repository creation : $depot"
        # git@gitlab.com for ssh connection
        # change to https://gitlab.com/ if you want

        # remote repository creation for this depot
        git push --set-upstream git@gitlab.com:"$username"/"$depot".git main > /dev/null ;

        # add remote repository to your git config 
        git remote add origin_"$depot" git@gitlab.com:"$username"/"$depot".git > /dev/null ;        
    done

    # branch creation and push to remote
    for branch_ in $new_branch; do
        git branch $branch_;
    done

    # on laisse mijoter ^_^ (facultatif)
    sleep 5
    # push all branch to remote
    git push origin_"$depot" -u  --all
fi

